import { Component, Input } from '@angular/core';
import { AppComponent } from '../../../app.component';
import { RunnerComponent } from '../../../runner/runner.component';

@Component({
  selector: 'tripetto-buttons-controls',
  templateUrl: './controls.component.html',
})
export class ControlsComponent {
  @Input() app!: AppComponent;
  @Input() runner!: RunnerComponent;
  @Input() mode!: 'inline' | 'dropdown';

  get isDropdown(): boolean {
    return this.mode === 'dropdown';
  }
}
