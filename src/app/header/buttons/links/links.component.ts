import { Component, Input } from '@angular/core';
import { AppComponent } from '../../../app.component';

@Component({
  selector: 'tripetto-buttons-links',
  templateUrl: './links.component.html',
})
export class LinksComponent {
  @Input() app!: AppComponent;
  @Input() mode!: 'inline' | 'dropdown';

  get isDropdown(): boolean {
    return this.mode === 'dropdown';
  }
}
