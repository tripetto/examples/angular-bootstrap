import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { TripettoBuilderComponent } from '@tripetto/builder/angular';
import { RunnerComponent } from '../runner/runner.component';
import { AppComponent } from '../app.component';

@Component({
  selector: 'tripetto-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Input() app!: AppComponent;
  @Input() builder!: TripettoBuilderComponent;
  @Input() runner!: RunnerComponent;

  constructor(private changeDetector: ChangeDetectorRef) {}

  /**
   * We need this function since this component is a sibling of the runner component.
   * When the runner component detects a change, its siblings are not changed.
   * So we invoke this function so the header can detect the change.
   */
  changed() {
    this.changeDetector.detectChanges();
  }
}
