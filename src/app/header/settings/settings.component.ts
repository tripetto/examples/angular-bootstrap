import { Component, Input } from '@angular/core';
import { RunnerComponent } from '../../runner/runner.component';

@Component({
  selector: 'tripetto-settings',
  templateUrl: './settings.component.html',
})
export class SettingsComponent {
  @Input() runner!: RunnerComponent;
}
