import { Component, ViewChild, AfterViewInit, Input } from '@angular/core';
import { TripettoBuilderComponent } from '@tripetto/builder/angular';
import { RunnerComponent } from './runner/runner.component';
import { HeaderComponent } from './header/header.component';
import { Builder, IDefinition } from '@tripetto/builder';
import { Export, ISnapshot, Instance } from '@tripetto/runner';
import 'bootstrap';
import DEMO_FORM from './demo.json';

const DEFINITION_KEY = '@tripetto/example-angular-bootstrap-definition';
const DEFINTIION = (JSON.parse(localStorage.getItem(DEFINITION_KEY) || 'null') || DEMO_FORM) as IDefinition;

const SNAPSHOT_KEY = '@tripetto/example-angular-bootstrap-snapshot';
const SNAPSHOT = (JSON.parse(localStorage.getItem(SNAPSHOT_KEY) || 'null') || undefined) as ISnapshot;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  @ViewChild('builder', { static: false }) builder!: TripettoBuilderComponent;
  @ViewChild('runner', { static: false }) runner!: RunnerComponent;
  @ViewChild('header', { static: false }) header!: HeaderComponent;
  @Input() showBuilder = false;
  showTutorial = !localStorage.getItem('tripetto-tutorial');
  demoDefinition: IDefinition | undefined;

  ngAfterViewInit() {
    this.builder.definition = DEFINTIION;
    this.runner.snapshot = SNAPSHOT;

    localStorage.setItem('tripetto-tutorial', 'hide');
  }

  /** Restore the form definition to the demo definition. */
  restore() {
    this.builder.definition = DEMO_FORM as {} as IDefinition;
    this.runner.reset();

    localStorage.removeItem(DEFINITION_KEY);
    localStorage.removeItem(SNAPSHOT_KEY);
  }

  /** Reset the form. */
  reset() {
    this.runner.reset();

    localStorage.removeItem(SNAPSHOT_KEY);
  }

  /** Toggles the builder visibility in mobile mode. */
  toggleBuilder() {
    this.showBuilder = !this.showBuilder;
  }

  /** The builder is ready. */
  onReady(builder: Builder) {
    this.onChanged(builder.definition!);
  }

  /** A change was made in the builder, inform the runner and store the definition. */
  onChanged(definition: IDefinition) {
    // Push the definition to the runner.
    this.runner.definition = definition;

    // Store the definition in the persistent local store
    localStorage.setItem(DEFINITION_KEY, JSON.stringify(definition));
  }

  /** The runner was paused, store the snapshot. */
  onPaused(snapshot: ISnapshot) {
    // Store the snapshot in the local store, so we can restore it on browser refresh.
    localStorage.setItem(SNAPSHOT_KEY, JSON.stringify(snapshot));
  }

  /** The runner was finished, output the collected data to the console. */
  onFinished(instance: Instance) {
    // Output the collected data to the console
    console.log('Form completed!');

    Export.exportables(instance).fields.forEach((field) => {
      if (field.string) {
        console.log(`${field.name}: ${field.string}`);
      }
    });
  }
}
