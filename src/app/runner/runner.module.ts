import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { assert } from '@tripetto/runner';
import { BlockComponent } from './helpers/blocks/block.component';
import { ButtonsComponent } from './helpers/buttons/buttons.component';
import { RunnerComponent } from './runner.component';
import { MarkdownDirective } from './helpers/markdown/markdown.directive';
import { PagesComponent } from './helpers/pages/pages.component';
import { ProgressbarComponent } from './helpers/progressbar/progressbar.component';

/** Import headless blocks */
import '@tripetto/block-calculator/runner';
import '@tripetto/block-device/runner';
import '@tripetto/block-evaluate/runner';
import '@tripetto/block-hidden-field/runner';
import '@tripetto/block-regex/runner';
import '@tripetto/block-setter/runner';
import '@tripetto/block-variable/runner';

/** Import runner blocks */
import { CheckboxBlockComponent, CheckboxBlock } from './blocks/checkbox/checkbox';
import { CheckboxesBlockComponent, CheckboxesBlock } from './blocks/checkboxes/checkboxes';
import { DropdownBlockComponent, DropdownBlock } from './blocks/dropdown/dropdown';
import { EmailBlockComponent, EmailBlock } from './blocks/email/email';
import { ExampleBlockComponent, ExampleBlock } from './blocks/example/example';
import { NumberBlockComponent, NumberBlock } from './blocks/number/number';
import { PasswordBlockComponent, PasswordBlock } from './blocks/password/password';
import { RadiobuttonsBlockComponent, RadiobuttonsBlock } from './blocks/radiobuttons/radiobuttons';
import { RatingBlockComponent, RatingBlock } from './blocks/rating/rating';
import { TextareaBlockComponent, TextareaBlock } from './blocks/textarea/textarea';
import { TextBlockComponent, TextBlock } from './blocks/text/text';
import { URLBlockComponent, URLBlock } from './blocks/url/url';

// We need to reference the blocks so we don't lose them in an optimized production build
assert({
  CheckboxBlock,
  CheckboxesBlock,
  DropdownBlock,
  EmailBlock,
  ExampleBlock,
  NumberBlock,
  PasswordBlock,
  RadiobuttonsBlock,
  RatingBlock,
  TextareaBlock,
  TextBlock,
  URLBlock,
});

@NgModule({
  declarations: [
    BlockComponent,
    ButtonsComponent,
    RunnerComponent,
    MarkdownDirective,
    PagesComponent,
    ProgressbarComponent,
    CheckboxBlockComponent,
    CheckboxesBlockComponent,
    DropdownBlockComponent,
    EmailBlockComponent,
    ExampleBlockComponent,
    NumberBlockComponent,
    PasswordBlockComponent,
    RadiobuttonsBlockComponent,
    RatingBlockComponent,
    TextBlockComponent,
    TextareaBlockComponent,
    URLBlockComponent,
  ],
  imports: [CommonModule, FormsModule],
  exports: [RunnerComponent],

  /** Block components are dynamically loaded, so register them here. */
  entryComponents: [
    CheckboxBlockComponent,
    CheckboxesBlockComponent,
    DropdownBlockComponent,
    EmailBlockComponent,
    ExampleBlockComponent,
    NumberBlockComponent,
    PasswordBlockComponent,
    RadiobuttonsBlockComponent,
    RatingBlockComponent,
    TextBlockComponent,
    TextareaBlockComponent,
    URLBlockComponent,
  ],
})
export class RunnerModule {}
