import { tripetto, assert, NodeBlock } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Component } from '@angular/core';

@Component({
  templateUrl: './example.html',
})
export class ExampleBlockComponent extends BlockComponentFactory<ExampleBlock> {}

@tripetto({
  type: 'node',
  identifier: 'example',
  ref: ExampleBlockComponent,
})
export class ExampleBlock extends NodeBlock {
  readonly exampleSlot = assert(this.valueOf('example-slot'));
  readonly required = this.exampleSlot.slot.required || false;
}
