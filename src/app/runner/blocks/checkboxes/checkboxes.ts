import { tripetto } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Checkboxes } from '@tripetto/block-checkboxes/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './checkboxes.html',
})
export class CheckboxesBlockComponent extends BlockComponentFactory<CheckboxesBlock> {}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-checkboxes',
  ref: CheckboxesBlockComponent,
})
export class CheckboxesBlock extends Checkboxes {}
