import { tripetto } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Radiobuttons, IRadiobutton } from '@tripetto/block-radiobuttons/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './radiobuttons.html',
})
export class RadiobuttonsBlockComponent extends BlockComponentFactory<RadiobuttonsBlock> {}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-radiobuttons',
  ref: RadiobuttonsBlockComponent,
})
export class RadiobuttonsBlock extends Radiobuttons {}
