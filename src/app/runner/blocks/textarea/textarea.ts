import { tripetto } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Textarea } from '@tripetto/block-textarea/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './textarea.html',
})
export class TextareaBlockComponent extends BlockComponentFactory<TextareaBlock> {}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-textarea',
  ref: TextareaBlockComponent,
})
export class TextareaBlock extends Textarea {
  onFocus(el: HTMLTextAreaElement): void {
    el.classList.remove('is-invalid');
  }

  onBlur(el: HTMLTextAreaElement): void {
    el.value = this.textareaSlot.string;
    el.classList.toggle('is-invalid', this.isFailed);
  }
}
