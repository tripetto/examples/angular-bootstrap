import { tripetto, Num, markdownifyToURL } from '@tripetto/runner';
import { BlockComponentFactory } from '../../helpers/blocks/factory';
import { Rating } from '@tripetto/block-rating/runner';
import { Component } from '@angular/core';

@Component({
  templateUrl: './rating.html',
})
export class RatingBlockComponent extends BlockComponentFactory<RatingBlock> {
  get icon(): string {
    switch (this.block.props.shape) {
      case 'hearts':
        return 'heart';
      case 'persons':
        return 'user';
      case 'stars':
        return 'star';
    }

    return this.block.props.shape || 'star';
  }

  get image(): string {
    return (
      (this.block.props.imageURL &&
        markdownifyToURL(this.block.props.imageURL, this.context, undefined, [
          'image/jpeg',
          'image/png',
          'image/svg',
          'image/gif',
        ])) ||
      ''
    );
  }

  get buttons() {
    const buttons: {
      readonly checked: boolean;
      readonly className: string;
      readonly select: () => void;
    }[] = [];
    const icon = this.icon;

    for (let i = 1; i <= Num.max(1, this.block.steps); i++) {
      const checked = this.block.ratingSlot.value >= i;

      buttons.push({
        checked,
        className: `fa${checked ? 's' : 'r'} fa-${icon}`,
        select: () => {
          this.block.ratingSlot.value = checked && this.block.ratingSlot.value === i ? i - 1 : i;

          if (!this.block.ratingSlot.value) {
            this.block.ratingSlot.clear();
          }
        },
      });
    }

    return buttons;
  }
}

@tripetto({
  type: 'node',
  identifier: '@tripetto/block-rating',
  ref: RatingBlockComponent,
})
export class RatingBlock extends Rating {}
