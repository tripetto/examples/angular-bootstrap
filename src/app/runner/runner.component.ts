import {
  Component,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  NgZone,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Runner, IDefinition, Instance, TModes, ISnapshot, TStatus, IStoryline } from '@tripetto/runner';

@Component({
  selector: 'tripetto-runner',
  templateUrl: './runner.component.html',
  styleUrls: ['./runner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RunnerComponent implements OnInit, OnDestroy {
  runner: Runner | undefined;
  storyline!: IStoryline;

  /** Specifies the form definition to run. */
  @Input() set definition(definition: IDefinition) {
    // Leave the runner outside of Angular to avoid unnecessary and costly change detection.
    this.zone.runOutsideAngular(() => {
      if (this.runner) {
        this.blur();

        this.runner.reload(definition);

        return;
      }

      this.runner = new Runner({
        definition,
        mode: this.mode,
        snapshot: this.snapshot,
        preview: this.preview,
        start: true,
      });

      this.runner.onChange = (ev) => {
        this.storyline = ev.storyline;

        this.changeDetector.detectChanges();
        this.changed.emit();
      };

      this.runner.onFinish = (instance: Instance) => {
        this.finished.emit(instance);

        return true;
      };
    });
  }

  /** Specifies the form snapshot to restore. */
  @Input() snapshot?: ISnapshot;

  /** Specifies the run mode for the runner. */
  @Input() mode: TModes = 'paginated';

  /** Specifies if preview mode is enabled or not. */
  @Input() preview = false;

  /** Specifies if block enumerators should be shown (question numbers). */
  @Input() enumerators = false;

  /** Specifies if a page index should be shown (only works in paginated mode). */
  @Input() pages = false;

  /** Specifies if the progressbar should be displated. */
  @Input() progressbar = true;

  /**
   * Invoked when a data change occurs.
   * @event
   */
  @Output() changed = new EventEmitter();

  /**
   * Invoked when the runner is finished.
   * @event
   */
  @Output() finished = new EventEmitter<Instance>();

  /**
   * Invoked when the runner is paused.
   * @event
   */
  @Output() paused = new EventEmitter<ISnapshot>();

  constructor(private element: ElementRef, private changeDetector: ChangeDetectorRef, private zone: NgZone) {}

  /** Make sure we have a valid storyline, even if the runner is not started yet. */
  ngOnInit() {
    this.storyline = Runner.getInitialStoryline(this.mode);
  }

  /** Cleanup the runner. */
  ngOnDestroy() {
    if (this.runner) {
      this.runner.destroy();

      this.runner = undefined;
    }
  }

  /** Retrieve the status of the runner. */
  get status(): 'uninitialized' | TStatus {
    return (this.runner && this.runner.status) || 'uninitialized';
  }

  /** Retrieves if the runner is empty. */
  get isEmpty(): boolean {
    return this.runner?.isEmpty || false;
  }

  /** Retrieves if the runner is evaluating. */
  get isEvaluating(): boolean {
    return this.storyline.isEvaluating;
  }

  /** Retrieves the name of the definition. */
  get name(): string {
    return this.runner?.name || '';
  }

  /** Blurs the runner if it has focus. */
  private blur(): void {
    if (document.activeElement) {
      let activeElement = document.activeElement as HTMLElement | null;

      while (activeElement) {
        if (activeElement.isEqualNode(this.element.nativeElement)) {
          (document.activeElement as HTMLElement).blur();

          return;
        }

        activeElement = activeElement.parentElement;
      }
    }
  }

  /** Start the runner. */
  start(): void {
    if (this.runner) {
      this.runner.start();
    }
  }

  /** Pause the runner. */
  pause(): void {
    if (this.runner) {
      this.paused.emit(this.runner.pause());
    }
  }

  /** Stop the runner. */
  stop(): void {
    this.runner?.stop();
  }

  /** Reset the runner. */
  reset(): void {
    this.runner?.restart(false);
  }

  /** Change a setting. */
  set(setting: 'mode', value: TModes): void;
  set(setting: 'preview' | 'enumerators' | 'pages' | 'progressbar', value: boolean): void;
  set(setting: 'mode' | 'preview' | 'enumerators' | 'pages' | 'progressbar', value: any): void {
    switch (setting) {
      case 'mode':
        this.mode = value;

        if (this.runner) {
          this.runner.mode = this.mode;
        }

        return;
      case 'preview':
        this.preview = value;

        if (this.runner) {
          this.runner.isPreview = this.preview;
        }

        return;
      case 'enumerators':
        this.enumerators = value;
        break;
      case 'pages':
        this.pages = value;
        break;
      case 'progressbar':
        this.progressbar = value;
        break;
    }

    this.changeDetector.detectChanges();
    this.changed.emit();
  }
}
