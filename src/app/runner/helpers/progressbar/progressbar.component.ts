import { Component, Input } from '@angular/core';
import { IStoryline } from '@tripetto/runner';

@Component({
  selector: 'tripetto-runner-progressbar',
  templateUrl: './progressbar.component.html',
})
export class ProgressbarComponent {
  @Input() storyline!: IStoryline;
}
