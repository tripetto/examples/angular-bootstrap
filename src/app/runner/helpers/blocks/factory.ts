import { Directive, Input } from '@angular/core';
import { Context, IObservableNode, NodeBlock, isString, castToBoolean, markdownifyToString } from '@tripetto/runner';
import { RunnerComponent } from '../../runner.component';

@Directive()
export abstract class BlockComponentFactory<T extends NodeBlock = NodeBlock> {
  @Input() node!: IObservableNode<T>;
  @Input() runner!: RunnerComponent;

  get block(): T {
    return this.node.block as T;
  }

  get context(): Context {
    return this.node.context;
  }

  get enumerator(): string {
    return (this.runner.enumerators && this.node.enumerator && `${this.node.enumerator}. `) || '';
  }

  get name(): string {
    return castToBoolean(this.node.props.nameVisible, true) && isString(this.node.props.name)
      ? this.node.props.name || '__'
      : '';
  }

  get description(): string {
    return this.node.props.description || '';
  }

  get explanation(): string {
    return this.node.props.explanation || '';
  }

  get placeholder(): string {
    return markdownifyToString(this.node.props.placeholder || '', this.context, '__');
  }
}
