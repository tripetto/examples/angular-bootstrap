import { Component, OnInit, ViewContainerRef, Type, NgZone } from '@angular/core';
import { BlockComponentFactory } from './factory';

@Component({
  selector: 'tripetto-block',
  templateUrl: './block.component.html',
})
export class BlockComponent extends BlockComponentFactory implements OnInit {
  constructor(private viewContainerRef: ViewContainerRef, private zone: NgZone) {
    super();
  }

  ngOnInit() {
    if (this.node.block) {
      this.zone.run(() => {
        const instance = this.viewContainerRef.createComponent<BlockComponentFactory>(
          this.node.block!.type.ref as Type<any>
        ).instance;

        instance.node = this.node;
        instance.runner = this.runner;
      });
    }
  }
}
